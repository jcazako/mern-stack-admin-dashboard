import express from "express";
import bodyParser from "body-parser";
import dotenv from "dotenv";

dotenv.config();

const app = express();

app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.all("*", (req, res) => {
  res.status(200).json({ appMe: "OK" });
});

app.listen(process.env.PORT, () =>
  console.log(`Listening to port ${process.env.PORT}`)
);
